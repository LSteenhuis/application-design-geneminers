/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package geneminers_new;

import java.util.ArrayList;

/**
 *
 * @author lsteenhuis
 */
public class GeneMiners {
    /**
     * @param args the command line arguments
     */
    public static void main(final String[] args) {
        GeneMiners gm = new GeneMiners();
        gm.start();
    }
    /**
     * the function start ensures the tests are being run.
     */
    public void start() {
    }

    /**
     * This function creates a Cluster object from the arrayList arrayOfGenes.
     * If this array appears to be empty an Exception will be thrown.
     * @param arrayOfGenes
     * @return boolean
     */
    public final boolean createCluster(final ArrayList arrayOfGenes) {
        try {
            if (arrayOfGenes == null) {
                throw new Exception();
            }
            ClusterGenes cg = new ClusterGenes();
            return true;
        } catch (Exception e) {
            System.err.println("could not create a cluster.");
            return false;
        }
    }
    
    /**
     * This function creates an Geneset object from a String called GeneDataset,
     * if Geneset can not be created an Exception will be thrown. if GeneDataset
     * appears to be empty an exception will be thrown
     * @param GeneDataset a dataset
     * @return boolean
     */
    public final boolean setExpressionDataset(final String GeneDataset) {
        try {
            Geneset gs = new Geneset();
            if (GeneDataset.length() == 0){
                throw new Exception();
            }
        } catch (Exception e) {
            System.err.println("Failed to create the gene dataset.");
            return false;
        }
        return true;
    }
    
     /**
     * searchGene needs a geneName and will search a list of genes to see if
     * the given gene is present. If the gene is found there will be a gene
     * object created if the gene is not found it will return false.
     * @param geneName the gene name
     * @return boolean
     */
    public final boolean searchGene(final String geneName) {
        ArrayList<String> listOfGenes = new ArrayList<>();
        listOfGenes.add("geneA");
        listOfGenes.add("geneB");
        listOfGenes.add("geneC");
        
        if (listOfGenes.contains(geneName)) {
            return true;
        } else {
            return false;
        }
    }
    /**
     * This function takes an arraylist , arrayOfGenes, and checks if this array
     * is empty. When the array does appear to be empty an exception is thrown.
     * @param arrayOfGenes
     * @return boolean
     */
    public final boolean createPlot(final ArrayList arrayOfGenes) {
        try {
            PlotGenes pg = new PlotGenes();
            if (arrayOfGenes == null) {
                throw new Exception();
            }
            return true;
        } catch (Exception e) {
            System.err.println("Could not create a plot.");
            return false;
        }
    }

}