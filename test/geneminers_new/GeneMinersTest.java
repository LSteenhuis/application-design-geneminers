package geneminers_new;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author eschutte
 */
public class GeneMinersTest {
    /**
     *
     */
    public GeneMinersTest() {
    }
    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }
    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }
    /**
     *
     */
    @Before
    public void setUp() {
    }
    /**
     *
     */
    @After
    public void tearDown() {
    }
    /**
     * Test of searchGene method, of class GeneMiners.
     */
    @Test
    public final void testSearchGene() {
        System.out.println("Testing the searchgene function");
        GeneMiners instance = new GeneMiners();
        // Test if the gene is found.
        boolean expResult = true;
        boolean result = instance.searchGene("geneA");
        assertEquals(expResult, result);
        // Test if the gene is not found.
        expResult = false;
        result = instance.searchGene("Dit is geen echt gen");
        assertEquals(expResult, result);
    }
    /**
     * Test of loadExpressionDataset method, of the class GeneMiners.
     */
    @Test
    public final void TestExpressionDataset() {
        System.out.println("Testing the setExpressionDataset");
        GeneMiners instance = new GeneMiners();
        // Test if the expressiondataset is created.
        boolean expResult = true;
        boolean result = instance.setExpressionDataset("DitIsEenDataset");
        assertEquals(expResult, result);
        // Test if the expressiondataset is not created.
        expResult = false;
        result = instance.setExpressionDataset("");
        assertEquals(expResult, result);
    }
    /**
     * Test of createPlot method, of class GeneMiners.
     */
    @Test
    public final void testCreatePlot() {
        System.out.println("Testing the createPlot function");
        GeneMiners instance = new GeneMiners();
        ArrayList<String> geneList = new ArrayList<>();
        geneList.add("Dit is lars zijn lievelings gen");
        geneList.add("Dit is erik zijn lieveligns gen");
        geneList.add("Dit gen is stom");
        geneList.add("Dit gen doet ons niet zoveel");
        // Test if the plot gets created.
        boolean expResult = true;
        boolean result = instance.createPlot(geneList);
        assertEquals(expResult, result);
        // Test if the plot gets not created.
        expResult = false;
        result = instance.createPlot(null);
        assertEquals(expResult, result);
    }
    /**
     * Test of createCluster method, of class GeneMiners.
     */
    @Test
    public final void testCreateCluster() {
        System.out.println("Testing the createCluster function");
        GeneMiners instance = new GeneMiners();
        ArrayList<String> geneList = new ArrayList<>();
        geneList.add("Dit is lars zijn lievelings gen");
        geneList.add("Dit is erik zijn lieveligns gen");
        geneList.add("Dit gen is stom");
        geneList.add("Dit gen doet ons niet zoveel");
        // Test if the cluster gets created.
        boolean expResult = true;
        boolean result = instance.createCluster(geneList);
        assertEquals(expResult, result);
        // Test if the cluster gets not created.
        expResult = false;
        result = instance.createCluster(null);
        assertEquals(expResult, result);
    }
}